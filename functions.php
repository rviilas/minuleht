﻿<?php

function kuva_reg_vorm(){	

	include_once("view/signup.html");
}
function kuva_kangelased(){

	include_once("view/kangelased.html");
}
function kuva_varustus(){

	include_once("view/varustus.html");
}
function kuva_pood(){
	
	include_once("view/pood.html");
}
function kuva_storm(){
	
	include_once("view/kangelased/storm_spirit.html");
}
function kuva_ember(){
	
	include_once("view/kangelased/ember_spirit.html");
}
function kuva_qop(){

	include_once("view/kangelased/queenofpain.html");
}
function kuva_tide(){

	include_once("view/kangelased/tidehunter.html");
}
function kuva_rubick(){

	include_once("view/kangelased/rubick.html");
}	
function kuva_slark(){
	
	include_once("view/kangelased/slark.html");
}
function kuva_puck(){
	
	include_once("view/kangelased/puck.html");
}
function kuva_mirana(){
	
	include_once("view/kangelased/mirana.html");
}
function kuva_magnus(){
	
	include_once("view/kangelased/magnus.html");
}
function kuva_ta(){
	
	include_once("view/kangelased/templar_assassin.html");
}

//sessiooni alustav funktsioon
function alusta_sessioon(){
	session_start();
}

//välja logimise funktsioon, mis suunab sessiooni lõpetamise funktsioonile
function logout(){
	global $myurl;
		lopeta_sessioon();
		header("Location: $myurl");
	}
	
function lopeta_sessioon(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
}

//kasutajaks registreerimise funktsioon
//võtab registreerimisväljade sisestused võrdleb pw1/pw2 ja kasutajanime pikkust
function register(){
 global $myurl;
 $username = $_POST['username'];
 $passwd1 = $_POST['passwd1'];
 $passwd2 = $_POST['passwd2'];
 
 if (empty($passwd1) || $passwd1 != $passwd2){
	header("Location: $myurl?mode=fail1");
	}
 else if(strlen($username) > 10 || empty($username)){
	header("Location: $myurl?mode=fail1");
	}
 else {
  echo "<p>Teie kasutajanimi on : '$username' </br>
		Teie salasõna on : '$passwd1' </p>";
		$_SESSION['passwd1']=$passwd1;
		$_SESSION['username']=$username;
 }

}

function autendi(){
	global $myurl;
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
		$username = $_POST['username'];
		$passwd = $_POST['passwd'];
		$userError = "";
		$pwError ="";
		$loginError = "";
}	
	if (empty($username)){
		$userError = "Kasutajanime sisestamine on kohustuslik<br><br>";
		echo $userError;
		header("Location: $myurl?mode=fail");
	}
	if (empty($passwd)){
		$pwError = "Salasõna sisestamine on kohustuslik<br><br>";
		echo $pwError;
		header("Location: $myurl?mode=fail");
	}
	
	if ((!empty($userError) ||!empty($pwError)) &&
		$username != $_SESSION['username'] && $passwd != $_SESSION['passwd1']){
		
		$loginError = "Sisestasite valed andmed.";
		echo $loginError;
		include_once("view/signup.html");
	}
	if ($_SESSION['username']==$username && $passwd==$_SESSION['passwd1']){
	
		header("Location: $myurl?mode=success");
 }
}

?>
