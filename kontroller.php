<?php 

require_once("functions.php");
$myurl=$_SERVER['PHP_SELF'];
alusta_sessioon();

$pood=array(			
	array("src"=>"pildid/pood/jin.png",        "alt"=>"Jin", 							"nimi"=>"Jin - Blade of Prosperity", 		"hind"=>"1€"),
	array("src"=>"pildid/pood/huo.png",    	   "alt"=>"Huo", 							"nimi"=>"Huo - Blade of Prosperity", 		"hind"=>"1€"),
	array("src"=>"pildid/pood/owl.png", 	   "alt"=>"Owlion", 						"nimi"=>"Stryff the Owlion ", 				"hind"=>"1.5€"),
	array("src"=>"pildid/pood/storm_robe.png", "alt"=>"Festive Robes of Good Fortune",  "nimi"=>"Festive Robes of Good Fortune", 	"hind"=>"1.5€"),
	array("src"=>"pildid/pood/ta_mask.png",    "alt"=>"Scarf of the Deadly Nightshade", "nimi"=>"Scarf of the Deadly Nightshade", 	"hind"=>"0.5€"),
	array("src"=>"pildid/pood/tide_skull.png", "alt"=>"Excavator's Treasure", 			"nimi"=>"Excavator's Treasure", 			"hind"=>"0.5€"),
);

$keha=array(	"0"=>"storm_body",		 "1"=>"ember_body",   "2"=>"qop_body", 
			    "3"=>"tidehunter_body",  "4"=>"rubick_body",  "5"=>"slark_body",
			    "6"=>"puck_body",  		 "7"=>"mirana_body",  "8"=>"magnus_body",
			    "9"=>"ta_body",    		 "10"=>"main_body"
);
 

$kangelased=array(
	array("href"=>"?mode=storm",		"big"=>"pildid/big/storm_big.png",		"small"=>"pildid/small/storm_spirit_small.png", 	"alt"=>"Storm Spirit"),
	array("href"=>"?mode=ember",		"big"=>"pildid/big/ember_big.png", 		"small"=>"pildid/small/ember_spirit_small.png", 	"alt"=>"Ember Spirit"),
	array("href"=>"?mode=qop",			"big"=>"pildid/big/qop_big.png", 		"small"=>"pildid/small/queenofpain_small.png", 		"alt"=>"Queen of Pain"),
	array("href"=>"?mode=tide",			"big"=>"pildid/big/tide_big.png", 		"small"=>"pildid/small/tidehunter_small.png", 		"alt"=>"Tidehunter"),
	array("href"=>"?mode=rubick",		"big"=>"pildid/big/rubick_big.png", 	"small"=>"pildid/small/rubick_small.png",			"alt"=>"Rubick"),
	array("href"=>"?mode=slark",		"big"=>"pildid/big/slark_big.png",		"small"=>"pildid/small/slark_small.png", 			"alt"=>"Slark"),
	array("href"=>"?mode=puck",			"big"=>"pildid/big/puck_big.png", 		"small"=>"pildid/small/puck_small.png", 			"alt"=>"Puck"),
	array("href"=>"?mode=mirana",		"big"=>"pildid/big/mirana_big.png",		"small"=>"pildid/small/mirana_small.png",			"alt"=>"Mirana"),
	array("href"=>"?mode=magnus",		"big"=>"pildid/big/mag_big.png", 		"small"=>"pildid/small/magnus_small.png", 			"alt"=>"Magnus"),
	array("href"=>"?mode=ta",			"big"=>"pildid/big/ta_big.png", 		"small"=>"pildid/small/templar_assassin_small.png", "alt"=>"Templar Assassin"),
 );
 
 
 $mode="pealeht";				
		
if (isset($_GET['mode']) && $_GET['mode']!=""){
	$mode=$_GET['mode'];
	}				

	
	include_once("view/head.html");
	
	switch($mode){
	case "aut":
		autendi();
	break;
	case "register":
		register();
	break;
	case "success":
		echo "<p>Sisselogimine õnnestus!</p>";
		include_once("view/kangelased.html");
	break;
	case "fail":
		echo "<p>Sisselogimine ebaõnnestus!</p>";
		kuva_reg_vorm();
	break;
	case "fail1":
		echo "<p>Registreerumine ebaõnnestus, kontrollige sisestatud andmeid!</p>";
		kuva_reg_vorm();
	break;
	case "logout":
		logout();
	break;
	case "signup":
		kuva_reg_vorm();
	break;
	case "kangelased":
		include_once("view/kangelased.html");
	break;
	case "varustus":
		kuva_varustus();
	break;
	case "pood":
		include_once("view/pood.html");
	break;
	case "storm":
		kuva_storm();
	break;
	case "ember":
		kuva_ember();
	break;
	case "qop":
		kuva_qop();
	break;
	case "tide":
		kuva_tide();
	break;
	case "rubick":
		kuva_rubick();
	break;
	case "slark":
		kuva_slark();
	break;
	case "puck":
		kuva_puck();
	break;
	case "mirana":
		kuva_mirana();
	break;
	case "magnus":
		kuva_magnus();
	break;
	case "ta":
		kuva_ta();
	break;
	

		default:
		include("view/pealeht.html");
	}
?>

